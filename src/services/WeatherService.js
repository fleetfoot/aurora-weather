class WeatherService {
	constructor(lat, long) {
		// this.startTime = 0;

		this.date = [];
		this.lowClouds = [];
		this.mediumClouds = [];
		this.highClouds = [];

		this.fetchLocationWeather(lat, long);
	}

	fetchLocationWeather(lat, long) {
		const url = `https://api.met.no/weatherapi/locationforecast/1.9/?lat=${lat}&lon=${long}`;

		return fetch(url)
			.then(response => response.text())
			.then(string => {
				return new window.DOMParser().parseFromString(
					string,
					"text/xml"
				);
			})
			.then(xmlWeather => this.getWeather(xmlWeather));
	}

	getWeather(xmlWeather) {
		let date = new Date();
		let hour = date.getHours();
		let lowClouds = 0;
		let mediumClouds = 0;
		let highClouds = 0;
		for (let i = 0; i < 48; i++) {
			date.setHours(hour + i);
			lowClouds = xmlWeather
				.getElementsByTagName("lowClouds")
				[i].getAttribute("percent");
			mediumClouds = xmlWeather
				.getElementsByTagName("mediumClouds")
				[i].getAttribute("percent");
			highClouds = xmlWeather
				.getElementsByTagName("highClouds")
				[i].getAttribute("percent");

			this.date.push(date.getHours());
			this.lowClouds.push(lowClouds);
			this.mediumClouds.push(mediumClouds);
			this.highClouds.push(highClouds);
		}
	}
}

export default WeatherService;
